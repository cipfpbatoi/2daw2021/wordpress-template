<?php get_header();?>
<section class="container">
	<?php
	isset($_GET["s"])? $palabra=$_GET["s"]:$palabra=NULL;
	isset($_GET["director"])? $director=$_GET["director"]:$director=NULL;
	isset($_GET["genero"])? $genero=$_GET["genero"]:$genero=NULL;
	
	$args=array(
		'post_type'=>'film',	
		's'=>$palabra,
		'meta_query'=>array(
			'relation'=>'AND',
			array(
				'key'=>'director',
				'value'=>$director,
				'compare'=>'LIKE'
			),
		)

	);

	query_posts($args);
	if(have_posts()) : while (have_posts()) : the_post(); ?>
	<div class="col-md-12">
		<h3><a href="<?php echo get_permalink(); ?>"> <?php the_title(); ?></a></h3>
		<?php the_excerpt();?>
	</div>
	<?php endwhile; else: ?>
	<p class="col-md-12">Lo sentimos, no se han encontrado películas que cumplan las condiciones de búsqueda.</p>
	<?php endif;?>
</section>
<?php get_footer(); ?>
