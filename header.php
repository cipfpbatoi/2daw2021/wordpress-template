<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title><?php bloginfo( 'name' ); wp_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri()?>/img/favicon.png">
    <?php wp_head(); ?>
</head>

<body id="body" <?php body_class('container-fluid'); ?> >

<header>
    <a href="<?php echo get_home_url()?>">
        <img src="<?php echo get_stylesheet_directory_uri()?>/img/logo.png" alt="Logo proyecto DIW" />
    </a>
    <nav>
        <?php wp_nav_menu( array( 'theme_location' => 'main', 'container' => false ) ); ?>
    </nav>
</header>

<main>
   
 