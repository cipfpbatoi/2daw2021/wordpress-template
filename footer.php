    </main>
    <footer>
        <?php wp_nav_menu( array( 'theme_location' => 'secondary', 'container' => false ) ); ?>
        <?php wp_footer(); ?>
        <p>Copyright &copy; 2020 - DIW</p>
    </footer>
</body>
</html>